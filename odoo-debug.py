#!/usr/bin/env python
import sys

sys.path.append('/opt/odoo/odoo-server/pycharm-debug.egg')

import pydevd
pydevd.settrace('10.0.1.6', port=5678, stdoutToServer=True, stderrToServer=True, suspend=False)

import openerp

if __name__ == "__main__":
    openerp.cli.main()
